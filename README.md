# Image Resizer Electron - studing
### for memory - do not doing
```shell
npx create-electron-app my-new-app
# or
yarn create electron-app my-new-app

# then
cd my-new-app
npm init -y
npm start
```
### auto-install NPM
```shell
npm install
```

## Cоздать и активировать виртуальное окружение:
```shell
python3 -m venv venv

source venv/bin/activate

python3 -m pip install --upgrade pip
```
git reset

### Установить зависимости из файла requirements.txt:
```python
pip3 install -r requirements.txt
pip3 install opencv-python
pip3 install imutils
```

### Record library tu requirements.txt
```python
pip3 freeze > requirements.txt
```
### OR manual install
```shell
npm install --save-dev electron-packager
npm install --save-dev electron
npm install -g @electron-forge/cli
npm install -s node-onvif
npm install --save-dev @electron-forge/maker-dmg
```
### For installing Python-shell 
```shell
npm i python-shell
```
```
* package.json
```json
  "dependencies": {
    "electron-squirrel-startup": "^1.0.0",
    "express": "^4.18.2",
    "node-onvif": "^0.1.7",
    "os": "^0.1.2",
    "path": "^0.12.7",
    "prompt-sync": "^4.2.0",
    "python-shell": "^5.0.0",
    "rtsp-relay": "^1.6.1"
  },
  "devDependencies": {
    "@electron-forge/cli": "^6.0.5",
    "@electron-forge/maker-deb": "^6.0.5",
    "@electron-forge/maker-dmg": "^6.0.5",
    "@electron-forge/maker-rpm": "^6.0.5",
    "@electron-forge/maker-squirrel": "^6.0.5",
    "@electron-forge/maker-zip": "^6.0.5",
    "electron": "23.0.0"
  }
```

### DEVELOP MODE - for automaticly restart electron
```javascript
npx electronmon .
```
### for packaging in APP (source: dir OUT)
```shell
npm run make
```
