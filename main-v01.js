const { app, BrowserWindow, ipcMain, shell} = require('electron');
const path = require('path');
const onvif = require("node-onvif");
const os = require("os");
const express = require("express");

process.env.NODE_ENV = 'production1'

const testCameras = [
    {
      "urn": "urn:uuid:00000000-0000-0000-0000-000000000000",
      "url": "rtsp://192.168.0.1:554/Streaming/Channels/101"
    },
    {
      "urn": "urn:uuid:00000000-0000-0000-0000-000000000001",
      "url": "rtsp://admin:19922002t@109.74.133.138:558/RVi/1/1"
    },
  ]

const testMessage = 'Найденные ONVIF устройства:\n\nurn:uuid:00000000-0000-0000-0000-000000000000: ' +
  'rtsp://192.168.0.1:554/Streaming/Channels/101\n\n' +
  'urn:uuid:00000000-0000-0000-0000-000000000001: rtsp://admin:19922002t@109.74.133.138:558/RVi/1/1\n\n'

const isDev = process.env.NODE_ENV !== 'production';
const isMac = process.platform === 'darwin';

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
// if (require('electron-squirrel-startup')) {
//   app.quit();
// }

let mainWindow
let streamWindow

const createMainWindow = () => {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    // width: isDev ? 1200 : 600,
    width: 600,
    height: 600,
    title: "ONVIF IP CAMERAS",
    icon: `${__dirname}/icons/mac/icon.icns`,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: true,
      preload: path.join(__dirname, '/renderer/js/preload.js'),
    },
  });

  // and load the index.html of the app.
  mainWindow.loadFile(path.join(__dirname, 'index.html'));

    // Open the DevTools.
  // if(isDev) {
  //   mainWindow.webContents.openDevTools();
  // }
};
const createStreamWindow = (url, port) => {
  // Create the browser window for stream video.
  streamWindow = new BrowserWindow({
    width: isDev ? 655 : 600,
    // width: 640,
    height: 525,
    title: `Source: ${url} on localhost:${port}`,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: true,
      preload: path.join(__dirname, '/renderer/js/preload.js'),
    },
  });
  console.log(`Opened StreamWindow with arg: ${url}`)
  // and load the .html of the app.
  streamWindow.loadURL(`http://localhost:${port}`)
  // streamWindow.loadFile(path.join(__dirname, '/renderer/stream.html'));

  // Open the DevTools.
  // if(isDev) {
  //   streamWindow.webContents.openDevTools();
  // }

    // Start server RTSP
  let app = ExpressServer(url, port)
  let server = app.listen(port, () => {
    console.log(`Server started on port: ${port}`);
  });

    // Stop server when Window closed
  streamWindow.on('closed', () => {
    // app.ws.close()

    server.close((err) => {
      console.log('server closed')
    })
    // app.ws()
    server = null
    streamWindow = null
  })

};

let count = 0
ipcMain.on('rtsp:windowOpen', (e,url) => {
  const ports = [4000, 4001, 4002, 4003, 4004, 4005]
  const port = ports[count]
  count ++
  createStreamWindow(url, port)

})

function ExpressServer(url, port) {
  const {
    PORT = port,
    BASE_PATH = `http://localhost:${PORT}`,
  } = process.env;

  const express = require('express');
  const app = express();
  const { proxy, scriptUrl } = require('rtsp-relay')(app);
  const handler = proxy({
    url: url, // 'rtsp://admin:19922002t@109.74.133.138:558/RVi/1/1',
    // if your RTSP stream need credentials, include them in the URL as above
    verbose: false,
    transport: 'tcp', // for best quality
  });

  // the endpoint our RTSP uses
  app.ws('/api/stream', handler);

  // this is an example html page to view the stream
  app.get('/', (req, res) =>
    // res.sendFile('./renderer/stream.html')
    res.send(`

      <canvas id="canvas" style="width:640px; height:480px" </canvas>

      <script src='${scriptUrl}'></script>
      <script>
        loadPlayer({
          url: 'ws://' + location.host + '/api/stream',
          canvas: document.getElementById('canvas')
        });
      </script>
`   ),
  );
  return app
  // return app.listen(PORT, () => {
  //   console.log('Ссылка на сервер');
  //   console.log(BASE_PATH);
  // });
}

ipcMain.on('scan:start', () => {
  checkNetwork();
});

// Code searching cameras
async function scanCameras() {
  console.log("Сканируем сеть на наличие камер с ONVIF...");
  // Datas for develop
  // if(isDev){
  //   return { success: true, message: testMessage, cameras: testCameras }
  // }

  // Ищем камеры в сети по протоколу ONVIF
  const devices = await onvif.startProbe();


  if (devices.length === 0) {
    return { success: false, message: 'No ONVIF cameras found.' };
  }

  let message = `Найдено ${devices.length} ONVIF устройства`;
  const cameras = [];
  for (const device of devices) {
    try {
      console.log(`Found device: ${device.urn}`);

      const camera = new onvif.OnvifDevice({
        xaddr: device.xaddrs[0],
        user : "admin",  // TODO Желательно вынести в input с GUI
        pass : "L277FAFE"
      });
      await camera.init();
      // const rowUrl = camera.rtspUri; // undefined
      const rowUrl = camera.getUdpStreamUrl()
      const url = `rtsp://admin:L277FAFE@${rowUrl.split('//')[1]}`

      console.log(`RTSP link: ${url}\n`);
      cameras.push({ urn: device.urn, url });
    } catch (error) {
      console.error(`Error retrieving RTSP link for device ${device.urn}: ${error}\n`);
      message += `Error retrieving RTSP link for device ${device.urn}: ${error}\n`;
    }
  }
  console.log(message, cameras)

  return { success: true, message, cameras };
};
async function checkNetwork() {
  const data = await scanCameras().then((data) => {
    console.log(data)
    mainWindow.webContents.send("scan:result", data)
  })
}

app.on('ready', () => {
  createMainWindow()

  mainWindow.on('closed', () => (mainWindow = null));
});

app.on('window-all-closed', () => {
  if (!isMac) {
    app.quit();
  }
});

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createMainWindow();
  }
});