const btn = document.querySelector('.app__btn-scan');
const message = document.querySelector('.app__message');

// Class for creating new DOM Element for each cameras
class Section {
  constructor({ renderer }, containerSelector) {
    this._renderer = renderer;
    this._container = document.querySelector(containerSelector);
  }
  appendItem(element) {
    this._container.append(element);
  }
  clear() {
    this._container.innerHTML = '';
  }
  renderItems(items) {
    this.clear(); // clearing all <li> elements before
    items.forEach(item => {
      this._renderer(item);
    });
  }
}

function getCameraElement(camera){
  const element = document.querySelector('#app__camera-template')
    .content.querySelector(".app__camera-item")
    .cloneNode(true);
  const newCameraElement = element
  const cameraBtn = newCameraElement.querySelector('.app__btn-camera-view')
  cameraBtn.innerHTML = `Model: ${camera.name}; s/n: ${camera.hardware}; ip: ${camera.xaddrs[0]}`

  // cameraBtn.disabled = inputUsername.value && inputPassword.value ? "" : "disabled"
  cameraBtn.addEventListener('click', () => handleViewCamera(camera, cameraBtn))
  // console.log(camera)
  return newCameraElement
}

// Instans of new camera element - miracle of creating cameras list :-)
const cameraList = new Section({
  renderer: (item) => {
      const cameraElement = getCameraElement(item);
      cameraList.appendItem(cameraElement);
    }
  }, ".app__cameras-list");

function handleViewCamera(camera, cameraBtn){
  const parentElement = cameraBtn.parentElement

  const username = parentElement.querySelector('#username').value
  const password = parentElement.querySelector('#password').value
  // const username = document.querySelector("#username").value
  // const password = document.querySelector('#password').value
  // console.log(camera, username, password)
  ipcRenderer.send('rtsp:CameraView', {
    camera,
    username,
    password
  })
}
ipcRenderer.on('rtsp:resultInit', (message) => {
  document.querySelector('.app__error').textContent = message
})

function handleScan() {
  // console.log("press btn")
  btn.innerText = "In process..."
  btn.disabled = "disabled"
  ipcRenderer.send('scan:start');
  ipcRenderer.on('scan:result', (data) => {
    // console.log(data)
    if(data.success){
      message.innerText = `${data.message}`;
      btn.innerText = "Scan Network"
      btn.disabled = ""
      const cameras = data.devices
      cameraList.renderItems(cameras)
    } else {
      message.innerText = `Result of scanning: ${data.message}`;
      btn.innerText = "Scan Network"
      btn.disabled = ""
    }
  })
}

btn.addEventListener('click', () => handleScan());