const onvif = require('node-onvif');
const prompt = require("prompt-sync")({ sigint: true });

console.log('Start the discovery process.');
// Find the ONVIF network cameras.
// It will take about 3 seconds.
const cams = []
onvif.startProbe().then((device_info_list) => {
  console.log(device_info_list.length + ' devices were found.');
  // Show the device name and the URL of the end point.
  device_info_list.forEach((info) => {
    cams.push(info.xaddrs[0])
  });
}).catch((error) => {
  console.error(error);
}).then(async () => {
  const username = prompt("Введите логин: ");
  const password = prompt("Введите пароль: ");
  for (let cam of cams) {
    let device = new onvif.OnvifDevice({
      xaddr: cam,
      user: username,
      pass: password
    });

// Initialize the OnvifDevice object
    try {
      await device.init().then((res) => console.log(res))
      console.log(device.getUdpStreamUrl())
    }
    catch (error) {
      console.log(error)
    }

  }
  prompt("Нажмите Enter для выхода");
})