import cv2
import imutils
from sys import argv


def run():

    rtsp_url = argv[1]

    # rtsp_url = 'rtsp://admin:L277FAFE@192.168.1.5:554/cam/realmonitor?channel=1&subtype=0&unicast=true&proto=Onvif?username=admin'

    rtsp_srteam = cv2.VideoCapture(rtsp_url)  # RTSP


    # frames = int(rtsp_srteamvideo.get(cv2.CAP_PROP_FRAME_COUNT))
    # width = int(rtsp_srteam.get(cv2.CAP_PROP_FRAME_WIDTH))
    # height = int(rtsp_srteam.get(cv2.CAP_PROP_FRAME_HEIGHT))
    # fps = rtsp_srteam.get(cv2.CAP_PROP_FPS)
    # print(frames, width, height, fps) # -1537228672809129 1280 720 15.0

    while(True):
        stream_ok, frame = rtsp_srteam.read()
        if frame is None:
            continue
        if stream_ok:
            # resize frame
            frame = imutils.resize(frame, width=640)
            # display current frame
            cv2.imshow('Press "q" for exit', frame)

            if cv2.waitKey(1) == ord('q'):
                break

    # clean ups
    cv2.destroyAllWindows()
    rtsp_srteam.release()


if __name__ == '__main__':
    print("Video from Python finished")
    run()
