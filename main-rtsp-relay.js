const { app, BrowserWindow, ipcMain, shell, Menu} = require('electron');
const path = require('path');
const onvif = require("node-onvif");
const os = require("os");

process.env.NODE_ENV = 'production'


const isDev = process.env.NODE_ENV !== 'production';
const isMac = process.platform === 'darwin';

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
// if (require('electron-squirrel-startup')) {
//   app.quit();
// }

let mainWindow
let streamWindow
let aboutWindow

const createMainWindow = () => {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: isDev ? 1400 : 800,
    height: 600,
    title: "ONVIF IP CAMERAS",
    icon: `${__dirname}/icons/mac/icon.icns`,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: true,
      preload: path.join(__dirname, '/renderer/js/preload.js'),
    },
  });

  // and load the index.html of the app.
  mainWindow.loadFile(path.join(__dirname, 'index.html'));

    // Open the DevTools.
  if(isDev) {
    mainWindow.webContents.openDevTools();
  }
};
const createStreamWindow = (url, port) => {
  // Create the browser window for stream video.
  streamWindow = new BrowserWindow({
    width: 660,
    height: 525,
    title: `Source: ${url} on localhost:${port}`,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: true,
      preload: path.join(__dirname, '/renderer/js/preload.js'),
    },
  });
  console.log(`Opened StreamWindow with arg: ${url}`)
  // and load the .html of the app.
  streamWindow.loadURL(`http://localhost:${port}`)
  // streamWindow.loadFile(path.join(__dirname, '/renderer/stream.html'));

  // Open the DevTools.
  // if(isDev) {
  //   streamWindow.webContents.openDevTools();
  // }

    // Start server RTSP
  let app = ExpressServer(url, port)
  let server = app.listen(port, () => {
    console.log(`Server started on port: ${port}`);
  });

    // Stop server when Window closed
  // streamWindow.on('closed', () => {
  //   // app.ws.close()
  //
  //   server.close((err) => {
  //     console.log('server closed')
  //   })
  //   // app.ws()
  //   server = null
  //   streamWindow = null
  // })

};

function createAboutWindow() {
  aboutWindow = new BrowserWindow({
    width: 300,
    height: 300,
    title: 'About CameraScan',
    icon: `${__dirname}/icons/mac/mac.png`,
  });

   aboutWindow.loadFile(path.join(__dirname, './renderer/about.html'));
  if(isDev) {
    mainWindow.webContents.openDevTools();
  }
}

// Menu template
const menu = [
  ...(isMac
    ? [
        {
          label: app.name,
          submenu: [
            {
              label: 'About',
              click: createAboutWindow,
            },
          ],
        },
      ]
    : []),
  {
    role: 'fileMenu',
  },
  ...(!isMac
    ? [
        {
          label: 'Help',
          submenu: [
            {
              label: 'About',
              click: createAboutWindow,
            },
          ],
        },
      ]
    : []),
  // {
  //   label: 'File',
  //   submenu: [
  //     {
  //       label: 'Quit',
  //       click: () => app.quit(),
  //       accelerator: 'CmdOrCtrl+W',
  //     },
  //   ],
  // },
  ...(isDev
    ? [
        {
          label: 'Developer',
          submenu: [
            { role: 'reload' },
            { role: 'forcereload' },
            { type: 'separator' },
            { role: 'toggledevtools' },
          ],
        },
      ]
    : []),
];

let count = 0

function ExpressServer(url, port) {
  const {
    PORT = port,
    BASE_PATH = `http://localhost:${PORT}`,
  } = process.env;

  const express = require('express');
  const app = express();
  const { proxy, scriptUrl } = require('rtsp-relay')(app);
  const handler = proxy({
    url: url, // 'rtsp://admin:19922002t@109.74.133.138:558/RVi/1/1',
    // if your RTSP stream need credentials, include them in the URL as above
    verbose: false,
    transport: 'tcp', // for best quality
  });

  // the endpoint our RTSP uses
  app.ws('/api/stream', handler);

  // this is an example html page to view the stream
  app.get('/', (req, res) =>
    // res.sendFile('./renderer/stream.html')
    res.send(`

      <canvas id="canvas" style="width:640px; height:480px" </canvas>

      <script src='${scriptUrl}'></script>
      <script>
        loadPlayer({
          url: 'ws://' + location.host + '/api/stream',
          canvas: document.getElementById('canvas')
        });
      </script>
`   ),
  );
  return app
  // return app.listen(PORT, () => {
  //   console.log('Ссылка на сервер');
  //   console.log(BASE_PATH);
  // });
}

ipcMain.on('scan:start', () => {
  checkNetwork();
});

// Code searching cameras
async function scanForCameras() {
  console.log("Сканируем сеть на наличие камер с ONVIF...");
  // Ищем камеры в сети по протоколу ONVIF
  const devices = await onvif.startProbe();
  // console.log(devices)

  if (devices.length === 0) {
    return {success: false, message: 'No ONVIF cameras found.\n'};
  }

  let message = `${devices.length} cameras were found in your network`;
  return { success: true, message, devices }// return List of Camera Object [{}, {}]
}

ipcMain.on('rtsp:CameraView', (e,data) => {
  getInitCamera(data).then((cameraInitResponse) => {
    // console.log(cameraInitResponse.url) // or undefined or url
    mainWindow.webContents.send("rtsp:resultInit", cameraInitResponse.message)
    const ports = [4000, 4001, 4002, 4003, 4004, 4005]
    const port = ports[count]
    count ++
    if (cameraInitResponse.url) {
      createStreamWindow(cameraInitResponse.url, 4000)
    }
  });
});

async function getInitCamera({ camera, username, password }) {
  let message;
  try {
    console.log(`Start Init Camera`, camera);

    const cam = new onvif.OnvifDevice({
      xaddr: camera.xaddrs[0],
      user : username,  // TODO Желательно вынести в input с GUI
      pass : password
    });
    await cam.init();
    const rowUrl = cam.getUdpStreamUrl()
    const url = `rtsp://${username}:${password}@${rowUrl.split('//')[1]}`
    // console.log(`GET RTSP link: ${url}\n`);
    return { message: `GET RTSP link: ${url}`, url }
  } catch (error) {
    console.error(`Error retrieving RTSP link for device ${camera.urn}: ${error}\n`);
    message = `${error}`;
  }
  return { message }
  // console.log(message, cameras)

};
async function checkNetwork() {
  const data = await scanForCameras().then((data) => {
    console.log(data)
    mainWindow.webContents.send("scan:result", data)
  })
}

app.on('ready', () => {
  createMainWindow()

  const mainMenu = Menu.buildFromTemplate(menu);
  Menu.setApplicationMenu(mainMenu);

  mainWindow.on('closed', () => (mainWindow = null));
});

app.on('window-all-closed', () => {
  if (!isMac) {
    app.quit();
  }
});

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createMainWindow();
  }
});
