const { app, BrowserWindow, ipcMain, shell, Menu} = require('electron');
const path = require('path');
const onvif = require("node-onvif");
const os = require("os");
let { PythonShell } = require('python-shell');

process.env.NODE_ENV = 'production'

const isDev = process.env.NODE_ENV !== 'production';
const isMac = process.platform === 'darwin';

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
// if (require('electron-squirrel-startup')) {
//   app.quit();
// }

let mainWindow
let aboutWindow

const createMainWindow = () => {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: isDev ? 1400 : 800,
    height: 600,
    title: "ONVIF IP CAMERAS",
    icon: `${__dirname}/icons/mac/icon.icns`,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: true,
      preload: path.join(__dirname, '/renderer/js/preload.js'),
    },
  });

  // and load the index.html of the app.
  mainWindow.loadFile(path.join(__dirname, 'index.html'));

    // Open the DevTools.
  if(isDev) {
    mainWindow.webContents.openDevTools();
  }
};

function createAboutWindow() {
  aboutWindow = new BrowserWindow({
    width: 300,
    height: 300,
    title: 'About CameraScan',
    icon: `${__dirname}/icons/mac/mac.png`,
  });

   aboutWindow.loadFile(path.join(__dirname, './renderer/about.html'));
  if(isDev) {
    mainWindow.webContents.openDevTools();
  }
}

// Menu template
const menu = [
  ...(isMac
    ? [
        {
          label: app.name,
          submenu: [
            {
              label: 'About',
              click: createAboutWindow,
            },
          ],
        },
      ]
    : []),
  {
    role: 'fileMenu',
  },
  ...(!isMac
    ? [
        {
          label: 'Help',
          submenu: [
            {
              label: 'About',
              click: createAboutWindow,
            },
          ],
        },
      ]
    : []),
  // {
  //   label: 'File',
  //   submenu: [
  //     {
  //       label: 'Quit',
  //       click: () => app.quit(),
  //       accelerator: 'CmdOrCtrl+W',
  //     },
  //   ],
  // },
  ...(isDev
    ? [
        {
          label: 'Developer',
          submenu: [
            { role: 'reload' },
            { role: 'forcereload' },
            { type: 'separator' },
            { role: 'toggledevtools' },
          ],
        },
      ]
    : []),
];

ipcMain.on('scan:start', () => {
  checkNetwork();
});

// Code searching cameras
async function scanForCameras() {
  console.log("Сканируем сеть на наличие камер с ONVIF...");
  // Ищем камеры в сети по протоколу ONVIF
  const devices = await onvif.startProbe();
  // console.log(devices)

  if (devices.length === 0) {
    return {success: false, message: 'No ONVIF cameras found.\n'};
  }

  let message = `${devices.length} cameras were found in your network`;
  return { success: true, message, devices }// return List of Camera Object [{}, {}]
}

ipcMain.on('rtsp:CameraView', (e,data) => {
  getInitCamera(data).then((cameraInitResponse) => {
    // console.log(cameraInitResponse.url) // or undefined or url
    mainWindow.webContents.send("rtsp:resultInit", cameraInitResponse.message)
    if (cameraInitResponse.url) {
      rtsp_url = cameraInitResponse.url
      console.log("function sendToPython", rtsp_url)

      // IMPORTANT - PATH TO PYTHON AND PATH TO SCRIPT
      let options = {
        mode: 'text',
        pythonPath: '/Users/olegvpc/Web/FW-Electron/cameras-scaner-electron/venv/bin/python3',
        pythonOptions: ['-u'], // get print results in real-time
        scriptPath: '/Users/olegvpc/Web/FW-Electron/cameras-scaner-electron/python/',
        args: [rtsp_url]
      };
      PythonShell.run('app.py', options)
        .then((messages)=> {
          console.log("from PythonShell", messages)
        })
        .catch((err) => console.log(err))
    }
  });
});

async function getInitCamera({ camera, username, password }) {
  let message;
  try {
    // console.log(`Start Init Camera`, camera);

    const cam = new onvif.OnvifDevice({
      xaddr: camera.xaddrs[0],
      user : username,  // TODO Желательно вынести в input с GUI
      pass : password
    });
    await cam.init();
    const rowUrl = cam.getUdpStreamUrl()
    const url = `rtsp://${username}:${password}@${rowUrl.split('//')[1]}`
    // console.log(`GET RTSP link: ${url}\n`);
    return { message: `GET RTSP link: ${url}`, url }
  } catch (error) {
    console.error(`Error retrieving RTSP link for device ${camera.urn}: ${error}\n`);
    message = `${error}`;
  }
  return { message }
  // console.log(message, cameras)

};
async function checkNetwork() {
  const data = await scanForCameras().then((data) => {
    // console.log(data)
    mainWindow.webContents.send("scan:result", data)
  })
}

app.on('ready', () => {
  createMainWindow()

  const mainMenu = Menu.buildFromTemplate(menu);
  Menu.setApplicationMenu(mainMenu);

  mainWindow.on('closed', () => (mainWindow = null));
});

app.on('window-all-closed', () => {
  if (!isMac) {
    app.quit();
  }
});

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createMainWindow();
  }
});